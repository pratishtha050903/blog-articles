# Sports and Hero-Worship

*Author: Vishal Raina*

![Ham-Ver Sportsmanship](images/hamver.webp)
Hey sports fanatics and fellow enthusiasts, buckle up as we dive into a captivating discussion about a rising phenomenon in the world of sports—hero worship—and its potential downsides that are shaking the very core of our beloved games. Today, we're peeling back the layers to explore how this admiration for sporting icons might be impacting the very essence of what makes sports so thrilling.


In the realm of sports, hero worship can manifest in various ways, from the adoration of individual athletes to the elevation of certain figures above the sport itself. This can lead to what we often refer to as a *"cult following,"* where fans become so enamoured with a particular athlete that their loyalty and support overshadow their appreciation for the sport as a whole.
![Dhoni finishes off in style](images/dhoni_six.webp)

Take, for instance, the world of cricket. In 2011, when India clinched the Cricket World Cup, the credit was overwhelmingly attributed to the captain at the time, MS Dhoni. While Dhoni undoubtedly played a crucial role, the collective effort of the team seemed to be overshadowed by the hero-worship directed at him. On the flip side, fast forward to the 2017 Champions Trophy final and the 2019 Cricket World Cup, where Virat Kohli faced undue criticism and backlash, so much so that calls for him to *relinquish his captaincy arose*. Hero worship has distorted the narrative, turning a team sport into a one-person show, with fans attributing success and failure solely to the captain.
![Kohli world cup semi final](images/kohli_2019_semi.webp)

Gautam Gambhir, known for his unfiltered opinions, succinctly captured the essence of Indian cricket culture when he remarked, **"In India, we worship individuals, not the team."** This sentiment, however, transcends cricket; it echoes across the spectrum of sports, finding resonance in the adrenaline-fueled realm of Formula 1.


The same trend is visible in Formula 1. Let me give you some context. In the realm of Formula 1, Lewis Hamilton and Mercedes held sway for *an uninterrupted seven years*, clinching seven consecutive Constructors' Championships and six Drivers' Championships between 2014 and 2020. However, 2021 marked a turning point, as the young prodigy Max Verstappen engaged in an epic battle, ultimately dethroning the reigning champions in an epic season finale at Abu Dhabi.


Fast forward to now, and Verstappen has not only reaffirmed his dominance but elevated his status to *a three-time world champion*. The 2023 season witnessed Verstappen's extraordinary prowess, with a staggering **19 wins out of 22 races**, breaking records that seemed unassailable. He shattered the longstanding nine-consecutive-wins record previously held by the legend Sebastian Vettel and set a new benchmark for the most laps led in a single season.

![Max Miami GP](images/max_1_closeup.webp)
Verstappen's triumph is not just a championship win; it's an assertion of dominance and a historic individual season, arguably the greatest in Formula 1 history. His relentless pursuit of excellence has rewritten the narrative, leaving an indelible mark on the sport's rich legacy. The emergence of new talents, showcasing unparalleled skill, has shifted the narrative. The success of these individuals is a testament to their skill and the efforts of their respective teams. Yet, instead of celebrating these incredible achievements, Verstappen often finds himself on the receiving end of *constant boos and negativity* from fans.


## Why does this happen?
 Hero worship. People become so fixated on their chosen favourites that they lose sight of the essence of the sport. They ignore the hard work, dedication, and skill that go into each victory, choosing instead to focus on their desire to see their favourite triumph. The danger lies in reducing the competition to a mere spectacle of who fans want to win, rather than appreciating the beauty of the competition itself.

We must learn to enjoy a sport for what it is—a showcase of human skill, determination, and teamwork. Appreciating greatness is not confined to a single individual; it extends to the entire field of competitors and the collective effort of teams. It's about recognizing that we are fortunate to witness these masters at work and understanding that each victory and defeat is part of the unpredictable and captivating nature of sports.
![Ronaldo GOAT](images/ronaldo_appreciate.webp)
In conclusion, let's embrace the unpredictability, savour the competition, and marvel at the skill on display. Here's to a future where genuine appreciation reigns supreme in the world of sports, where the pursuit of excellence is revered, and the thrill lies not just in the victories but in the relentless journey of every athlete and every team.

# Connect with me

https://www.instagram.com/vishal.llll/

https://www.linkedin.com/in/vishal-raina-487254287/

